const std = @import("std");

pub fn build(b: *std.Build) void {

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardOptimizeOption(.{});
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    const lib = b.addStaticLibrary(.{
        .name="toml-zig",
        .root_source_file=.{.path="src/main.zig"},
        .optimize=mode,
        // .optimize=std.builtin.Mode.Debug,
        .target=target});
    // const lib = b.addStaticLibrary("toml-zig", "src/main.zig");
    // lib.setBuildMode(mode);
    lib.install();

    const exe = b.addExecutable(.{
        .name="bintest",
        .root_source_file=.{.path="src/bin.zig"},
        .optimize=mode,
        .target=target});
    // exe.setTarget(target);
    // exe.setBuildMode(mode);
    exe.install();
    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
    const main_tests = b.addTest(.{.name="maintest",.root_source_file=.{.path="src/main.zig"}});
    // main_tests.setBuildMode(mode);

    // const dateTime_tests = b.addTest("src/Value/datetime.zig");
    // dateTime_tests.setBuildMode(mode);

    // const boolean_tests = b.addTest("src/Value/boolean.zig");
    // boolean_tests.setBuildMode(mode);

    // const string_tests = b.addTest("src/Value/string.zig");
    // string_tests.setBuildMode(mode);

    // const float_tests = b.addTest("src/Value/float.zig");
    // float_tests.setBuildMode(mode);

    // const integer_tests = b.addTest("src/Value/integer.zig");
    // integer_tests.setBuildMode(mode);

    // const value_tests = b.addTest("src/value.zig");
    // value_tests.setBuildMode(mode);

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
    // test_step.dependOn(&dateTime_tests.step);
    // test_step.dependOn(&boolean_tests.step);
    // test_step.dependOn(&string_tests.step);
    // test_step.dependOn(&float_tests.step);
    // test_step.dependOn(&integer_tests.step);
    // test_step.dependOn(&value_tests.step);
}
