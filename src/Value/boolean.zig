const std = @import("std");
const testing = std.testing;

pub fn isBoolean(input: []const u8) bool {
    return std.mem.eql(u8, input, "true") or std.mem.eql(u8, input, "false");
}

pub fn toBool(input: []const u8) bool {
    if (std.mem.eql(u8, input, "true")) {
        return true;
    }
    if (std.mem.eql(u8, input, "false")) {
        return false;
    } else {
        unreachable;
    }
}

test "Test False" {
    try testing.expect(isBoolean("true"));
}

test "Test False" {
    try testing.expect(isBoolean("false"));
}

test "Test Capitalization" {
    try testing.expect(!isBoolean("False"));
}

test "Test Garbage" {
    try testing.expect(!isBoolean("falll"));
}

test "Convert To Bool" {
    try testing.expectEqual(bool, @TypeOf(toBool("true")));
}
