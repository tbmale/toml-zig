# toml-zig

A TOML 1.0.0 deserialization/serialization library for Zig

## Status

#### Current TOML 1.0 Spec Status:

- [x] Strings + all escapes, String Literals, Multiline Strings, Multiline String Literals.
- [x] Bools
- [x] DateTime (via private DateTime implementation with the methods for EPOCH/timestamp conversion)
- [x] Floats
- [x] Integers
- [x] Arrays
- [x] Tables
- [x] Inline Tables
- [x] Array Tables
- [x] Deserialization of all above
- [x] Subtables support (dots)
- [x] Serialization
- [x] Comments



## Pull Requests

Feel free to submit any pull request regarding new features or behavior change.

